# -*- coding: utf-8 -*-
from openerp.osv import osv,fields
from openerp import tools

class fleet_vehicle_model(osv.Model):

    def _model_name_get_fnc(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            name = record.modelname
            if record.brand_id.name:
                name = record.brand_id.name + ' / ' + name
            res[record.id] = name
        return res

    def on_change_brand(self, cr, uid, ids, model_id, context=None):
        if not model_id:
            return {'value': {'image_medium': False}}
        brand = self.pool.get('sahara.vehicle.model.brand').browse(cr, uid, model_id, context=context)
        return {
            'value': {
                'image_medium': brand.image,
            }
        }

    _name = 'sahara.vehicle.model'
    _description = 'Model of a vehicle'
    _order = 'name asc'

    _columns = {
        'name': fields.function(_model_name_get_fnc, type="char", string='Name', store=True),
        'modelname': fields.char('Model name', required=True), 
        'brand_id': fields.many2one('sahara.vehicle.model.brand', 'Model Brand', required=True, help='Brand of the vehicle'),
        'vendors': fields.many2many('res.partner', 'fleet_vehicle_model_vendors', 'model_id', 'partner_id', string='Vendors'),
        'image': fields.related('brand_id', 'image', type="binary", string="Logo"),
        'image_medium': fields.related('brand_id', 'image_medium', type="binary", string="Logo (medium)"),
        'image_small': fields.related('brand_id', 'image_small', type="binary", string="Logo (small)"),
    }


class fleet_vehicle_model_brand(osv.Model):
    _name = 'sahara.vehicle.model.brand'
    _description = 'Brand model of the vehicle'

    _order = 'name asc'

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'name': fields.char('Brand Name', required=True),
        'image': fields.binary("Logo",
            help="This field holds the image used as logo for the brand, limited to 1024x1024px."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized photo", type="binary", multi="_get_image",
            store = {
                'sahara.vehicle.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized logo of the brand. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved. "\
                 "Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Smal-sized photo", type="binary", multi="_get_image",
            store = {
                'sahara.vehicle.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized photo of the brand. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
    }
    
    
  
class propose_icence (osv.osv):
    _name="propose.licence"
    _columns = {
                'name':fields.char('Title'),
                'code':fields.char('Code'),
                }
    
class configuration_period (osv.osv):
    _name="configuration.period"
    _columns = {
                'name':fields.char('Period'),
                'nb_days':fields.integer('Number of Days')
,                }
    
    _defaults ={
               'nb_days':365, 
                }
    
class configuration_extension(osv.osv):
    _name="configuration.extension"
    _columns = {
                'name':fields.char('Object Extension'),
                }
    
class configuration_type_vehicle(osv.osv):
    _name="configuration.type_vehicle"
    _columns = {
                'name':fields.char('Type vehicle'),
                }

class configuration_cost_methode(osv.osv):
    _name="configuration.cost_methode"
    _columns = {
                'name':fields.char('Cost methode'),
                }

class configuration_scope_purpose(osv.osv):
    _name="configuration.scope_purpose"
    _columns = {
                'name':fields.char('Scope of purpose'),
                }

