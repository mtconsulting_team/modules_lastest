# -*- coding: utf-8 -*-
{
"name": "Sahara Mondatory Insurance",
"version": "1.0",
"depends": ["base","sahara_insurance_sales","sahara_libya_cities"],
"author": "Helmi Dhaoui",
"category": "Category",
"description": """ Sahara Mondatory Insurance """,

"init_xml":[],
'data': [
         "data/configuration.xml",
         "views/configuration.xml",
         "views/mondatory_insurance_policy.xml",
#          "data/sequence.xml",
         "rapport/rapport_mondatory_insurance_policy.xml",
         "rapport/mandatory_insurance_policy_workflow.xml",
           ],
'demo': [],
'test': [],
'installable': True,

}
