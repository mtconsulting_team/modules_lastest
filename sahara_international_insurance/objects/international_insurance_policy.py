# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp import api
import time
from datetime import datetime, timedelta , date
from mx import DateTime
from _smbc import Context

class international_insurance(osv.osv):
    _name = "international.insurance.policy"
    
    def compute_values(self, cr, uid, ids, context=None):
        current_obj = self.browse(cr, uid, ids, context)
        type_obj=current_obj.type_viehcule_insurance_id
        value_vieh_hor_p=current_obj.viehcule_horses_power
        value_vieh_load_t=current_obj.viehcule_load_tonnage
        value_vieh_nbr_tonn=current_obj.viehcule_number_tonnage
        res=0
        BC=type_obj.cost_method
        les_b_c=type_obj.base_calcule_setting_ids
        viehcule_load_tonnage = current_obj.viehcule_load_tonnage
        viehcule_number_tonnage = current_obj.viehcule_number_tonnage
        if BC=='fixed_amount' :
                                res+= (current_obj.period + type_obj.value)/type_obj.period
                     
        if  BC == 'load_tournage' :
                        pricing_values_id = self.pool.get('pricing.values').search(cr,uid,[('base_calcule_id','=',type_obj.id),('min','<=',viehcule_load_tonnage),('max','>=',viehcule_load_tonnage)],context=context)
                        if pricing_values_id and len(pricing_values_id):
                            pricing_values = self.pool.get('pricing.values').browse(cr,uid,pricing_values_id[0],context)
                            res+= (current_obj.period + pricing_values.overvalue)/type_obj.period
                         
        if  BC == 'number_passengers' :
                        pricing_values_id = self.pool.get('pricing.values').search(cr,uid,[('base_calcule_id','=',type_obj.id),('min','<=',viehcule_number_tonnage),('max','>=',viehcule_number_tonnage)],context=context)
                        if pricing_values_id and len(pricing_values_id):
                            pricing_values = self.pool.get('pricing.values').browse(cr,uid,pricing_values_id[0],context)
                            res+= (current_obj.period + pricing_values.overvalue)/type_obj.period
                
        res=res-res*(current_obj.discount/100)    

        net= res *0.01
        roud=round(net)
        if roud<net:
            tax=roud+0.5 
        else:
            tax=roud 
        
        values = {
                  'net_premium':res,
                  'tax':tax,
                  'adv_fees':0.005*res,
                  'stamp_duty':0.250,
                  'issue_fees':3,
                  'gross_premium':res+3+tax+0.005*res+0.25,
        }        
        self.write(cr, uid, ids, values, context)
        return  {'value' : values}
    
    def onchange_partner(self, cr, uid, ids, partner_id, context=None):
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
            value = {
                   'is_company':partner.is_company,
                   'partner_proffission':partner.function,
                   'partner_phone':partner.phone,
                   'partner_adress':partner.street,
                   'partner_email':partner.email,
                   }       
            return {'value':value}  
        
        
    def onchange_periode(self, cr, uid, id, start_date, period, val, context=None):
        if val == 1 :
            start_date = date.today() + timedelta(days=int(1))
            start_date = datetime.strptime(str(start_date), "%Y-%m-%d")
            return {'value' : {'start_date':start_date}}
        if start_date and period:
            start_date = datetime.strptime(start_date, "%Y-%m-%d")
            end_date = start_date + timedelta(period)
            return {'value' : {'end_date' : end_date}}
  
    def _check_date(self, cr, user, ids): 
        obj = self.browse(cr, user, ids[0])
        date = obj.start_date
        if date:
            date = datetime.strptime(date, "%Y-%m-%d")
            if date < date.today() and date:
                    return False
        return True
    _constraints = [(_check_date, 'Error: Coverage Start day must be after today', ['start_date']) ]     

    _columns = {
                'code':fields.char('code'),
                'name':fields.char('Policy No', required=True),
                'date':fields.date('Date'),
                'partner_id':fields.many2one('res.partner', "Partner", required=True),
                'commercial_id':fields.many2one('res.partner', "", required=False),
                'ref_to_commercial':fields.boolean('Ref to a commercial', required=False),
                'is_company':fields.boolean('Is company'),
                'country_id': fields.many2one('res.country', 'Country'),
                'state_id': fields.many2one('res.country.state', 'City'),
                'street': fields.char('Street Address'),
                'zip': fields.char('ZIP'),
                'street2': fields.char('Street'),
                'partner_proffission':fields.many2one('international.proffission','Profossion'),
                'partner_phone':fields.char('Phone'),
                'partner_email':fields.char('Email'),
                'partner_nationality':fields.many2one('res.company','Nationality'),
                'sector':fields.many2one('international.sector','Sector'),
                'partner_vat':fields.char('VAT'),
                'partner_register_no':fields.char('Register No'),
                
                'period':fields.integer('Period'),
                'start_date':fields.date('From Date/Time', required=True),
                'end_date':fields.date('To Date/Time'),
                
                'type_viehcule_insurance_id':fields.many2one('international.type.viehcule.insurance', "Type of Viehcule Insurance", required=True),
                'vehicle_model_id':fields.many2one('sahara.vehicle.model', "Vehicle model", required=True),
                'vehicle_model_brand_id':fields.many2one('sahara.vehicle.model.brand', "Vehicle model Brand", required=True),
                'year_model':fields.char('Year Model', required=True),
                'color':fields.many2one('veh.color', 'Color', required=True),
                'vin':fields.char('VIN', required=True),
                'plate_no':fields.char('Plate NO', size=2, required=True),
                'plate_no2':fields.char('Plate NO', required=True),
                'place_reg':fields.many2one('res.country.state', 'Place Reg', required=True),
                'date_reg':fields.char('Date Register', required=False),
                'porpose_liscence':fields.many2one('propose.licence', string="Propose of Liscence", required=True),
                'viehcule_horses_power': fields.integer('Viehcule Horses Power', required=True),
                'viehcule_load_tonnage': fields.integer('Viehcule Load Tonnage', required=True),
                'viehcule_number_tonnage': fields.integer('Viehcule Number of Passengers', required=True),
                'with_trailer':fields.boolean('With Trailer'),
                'qty': fields.integer('QTY'),
                'trailer_load_tonnage': fields.integer('Trailer Load Tonnage'),
                'trailer_number_tonnage': fields.integer('Trailer Number of Passengers'),
                
                'net_premium':fields.float('Net Premium', digits=(16, 3)),
                'tax':fields.float('Tax', digits=(16, 3)),
                'adv_fees':fields.float('Suppervision Fees', digits=(16, 3)),
                'stamp_duty':fields.float('Stamps ', digits=(16, 3)),
                'issue_fees':fields.float('Issuance Fees', digits=(16, 3)),
                'gross_premium':fields.float('Total Premium', digits=(16, 3)),
                'discount': fields.float('Discount', digits=(16,3)),
                'state':fields.selection([
                    ('draft', 'Draft'),
                    ('confirm', 'Confirm'),
                    ('paid', 'Pay'),
                    ('done', 'Done'),
                     ], 'Status', select=True, default='draft'),
                }
    
    _defaults = {   
        'start_date': date.today().strftime('%Y-%m-%d'),
        'name': lambda obj, cr, uid, context: '/',
        'state':'draft',
        }
    
    
    def on_change_plate_no(self, cr, uid, ids, plate_no, context=None):
        if plate_no:
            state = self.pool.get('res.country.state').search(cr,uid,[('code','=',plate_no)],context=context)
            if state and len(state)>0 :
                return {'value': {'place_reg': state[0]}}

     
    def validate(self, cr, uid, ids, *args):
#         vals['valid']=True
#         return super(travel_insurance_policy, self).write(cr, uid, vals, context=context)
        return self.write(cr, uid, ids, {'state':'paid'})
    
    def payer(self, cr, uid, vals, context=None):
        
        if context is None:
            context = {}
        current_obj = self.browse(cr, uid, vals, context)

        if current_obj.name == '/':
            code = self.pool.get('res.users').browse(cr, uid, uid).company_id.code
        if not code:
            code = "01"
        
        values = {
                  'name':str(code) + self.pool.get('ir.sequence').get(cr, uid, 'insurance.policy', context=context) or '/',
                  'code':str(code),
                  'state':'done',
                }        
        return self.write(cr, uid, vals, values, context)
    
    @api.multi
    def print_insurance(self):
        return self.env['report'].get_action(self, 'sahara_mandatory_insurance.rapport_international_insurance_policy')

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
#         
#         if vals.get('name', '/') == '/':
#             code = self.pool.get('res.users').browse(cr, uid, uid).company_id.code
#         if not code:
#             code = "01"
#             vals['name'] = str(code) + self.pool.get('ir.sequence').get(cr, uid, 'insurance.policy', context=context) or '/'
#         vals['code'] = str(code)
        vals['state'] = 'confirm'
        return super(international_insurance, self).create(cr, uid, vals, context=context)
    
class type_viehcule_insurance(osv.osv):
    _name = "international.type.viehcule.insurance"
    
    _columns = {
                'name':fields.char('Name', translate=True),
                'period':fields.integer('Period(Jour)'),
                'type_car_id':fields.many2one('type.car','Type of Viehcule'),
                'extension':fields.selection([('trailer', ' Trailer')], "Extension"),
                'cost_method':fields.selection([('fixed_amount', 'Fixed Amount'),
                                                ('number_passengers', 'By Number of Passengers'),
                                                ('load_tournage', 'By Load Tonnage'),
                                                ('nbr_passengers_load_tournage', 'By Load Tonnage & Nbr of Passengers')], "Cost Method Basic"),
                'fixed':fields.boolean('Fixed', required=False),
                'value': fields.float('value', digits=(16, 3)),
                'note': fields.text('Description'),
                'code':fields.char('Code', size=64, required=False, readonly=False),
                'base_calcule_setting_ids':fields.one2many('pricing.values', 'base_calcule_id', 'Pricing And Values', required=False),
#                 'base_calcule_setting2_ids':fields.one2many('sahara.base.calcul', 'base_calcule_id', 'Pricing And Values 2', required=False,limit=2),

            }
    defaults={
              'period':1
              }
    
    def on_change_name(self, cr, uid, id, type_car_id, extension,context=None):
        res = {}
        if type_car_id and extension :
            name = ''
            type_car = self.pool.get('type.car').browse(cr,uid,type_car_id,context)
            name = type_car.name + ' / ' + extension
            res = {'value' : {'name':name}}
        else :
            name = ''
            type_car = self.pool.get('type.car').browse(cr,uid,type_car_id,context)
            name = type_car.name 
            res = {'value' : {'name':name}}
        
        return  res

class type_car(osv.osv):
    _name = 'type.car' 
    _columns = {
            'name':fields.char('Name', size=64, required=True, readonly=False),
    }

    
class place_register(osv.osv):
    _name = 'place.register' 
    _columns = {
            'name':fields.char('Name', size=64, required=False, readonly=False),
            'code':fields.char('Code', size=64, required=False, readonly=False),
    }
class veh_color(osv.osv):
    _name = 'veh.color' 
    _columns = {
            'name':fields.char('Name', size=64, required=False, readonly=False),
            'code':fields.char('Code', size=64, required=False, readonly=False),
    }
class res_company(osv.osv):
    _inherit = 'res.company' 
    
    _columns = {
                'code':fields.char('code'),
                }
class SaharaPricingValues(osv.osv):
    _name = 'pricing.values' 
    _columns = {
           
            'min': fields.float('Min', digits=(16, 3)),
            'max': fields.float('Max', digits=(16, 3)),
#             'preniumvalue': fields.float('Prenium Value', digits=(16, 3)),
            'overvalue': fields.float('Over Value', digits=(16, 3)),    
            'base_calcule_id':fields.many2one('international.type.viehcule.insurance', 'type viehcule', required=False),        
#             'pricing_value_id':fields.many2one('sahara.base.calcul', 'Base of Calcule', required=False),
        }
    def on_change_basecalcule(self, cr, uid, id, basecalcule, context=None):
        
        return  {'value' : {'Basecalcule':basecalcule}}
class SaharaBaseCalcul(osv.osv):
    _name = 'sahara.base.calcul'
    _columns = {
                'Basecalcule':fields.selection([
                ('Horses Power', 'Horses Power'),
                ('Load Tonnag', 'Load Tonnag'),
                ('Number of Passengers', 'Number of Passengers')
                ], 'Base Of Calcule :', select=True),
#                 'pricing_values_ids':fields.one2many('pricing.values', 'pricing_value_id', 'pricing', required=False),
                'base_calcule_id':fields.many2one('international.type.viehcule.insurance', 'type viehcule', required=False), 
            }

#-------------------------------------------------------------------------------
# international_sector
#-------------------------------------------------------------------------------
class international_sector(osv.osv):
    _name = 'international.sector' 
    _columns = {
            'name':fields.char('Name', size=64, required=False),
    }
    
    
class international_proffission(osv.osv):
    _name = 'international.proffission' 
    _columns = {
            'name':fields.char('Name', size=64, required=False),
    }
