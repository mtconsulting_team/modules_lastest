# -*- coding: utf-8 -*-
{
"name": "Sahara International Insurance",
"version": "1.0",
"depends": ["base","sahara_insurance_sales","sahara_libya_cities"],
"author": "Mohamed Moussa",
"category": "Category",
"description": """ Sahara International Insurance """,

"init_xml":[],
'data': [
         "data/donnee.sql",
         "data/configuration.xml",
         "data/type_car_data.xml",
         "data/international.type.viehcule.insurance.csv",
         "data/donnee.sql",
         "views/configuration.xml",
         "views/international_insurance_policy.xml",
#          "data/sequence.xml",
         "rapport/rapport_international_insurance_policy.xml",
         "rapport/international_insurance_policy_workflow.xml",
           ],
'demo': [],
'test': [],
'installable': True,

}
