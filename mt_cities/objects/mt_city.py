# -*- coding: utf-8 -*-


from openerp.osv import osv, fields

ZIP_TYPES = [('10', '10XX'),
             ('11', '11XX'),
             ('12', '12XX'),
             ('20', '20XX'),
             ('21', '21XX'),
             ('22', '22XX'),
             ('30', '30XX'),
             ('31', '31XX'),
             ('32', '32XX'),
             ('40', '40XX'),
             ('41', '41XX'),
             ('42', '42XX'),
             ('50', '50XX'),
             ('51', '51XX'),
             ('60', '60XX'),
             ('61', '61XX'),
             ('70', '70XX'),
             ('71', '71XX'),
             ('80', '80XX'),
             ('81', '81XX'),
             ('90', '90XX'),
             ('91', '91XX')]

class mt_city(osv.osv):
   
    _name = "mt.city"
    _description = "City"

    def _get_name(self, cr, uid, ids, field_name, arg, context):
        result={}
        for city in self.browse(cr, uid, ids, context=context):
            result[city.id] = '%(zip)s %(city)s' % {'zip': city.zip or '' ,'city': city.short_name or ''}
        return result

    _columns = {
        'zip_type': fields.selection(ZIP_TYPES, 'Zip types'),
        'zip': fields.char('Zip', size=10,translate=True),
        'zip_complement': fields.char('Zip complement', size=2,translate=True),
        'short_name': fields.char('Short name', size=18,translate=True),
        'long_name': fields.char('Long name', size=27,translate=True),
        'state_id': fields.many2one('res.country.state', 'State'),
        'country_id': fields.many2one('res.country', 'Country'),
        'name': fields.function(_get_name, type='char', method=True, store=True, string='Name'),
    }
    
    _order = 'country_id, name asc'
                    
mt_city()

class mt_partner(osv.osv):

    _inherit = 'res.partner'
    _description = "mt City Partner"

    _columns = {
        'city_id': fields.many2one('mt.city', 
                                   'City Search',
                                   required=False, store=False),
    } 

    def onchange_city_id(self, cr, uid, ids, city_id): 
        v={}
        
        if city_id:
            city = self.pool.get('mt.city').browse(cr, uid, city_id)
            v['zip'] = city.zip
            v['city'] = city.long_name
            v['country_id'] = city.country_id.id 
            v['state_id'] = city.state_id.id 
        
        return {'value': v} 
    
mt_partner()
