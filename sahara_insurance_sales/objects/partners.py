# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
import time
from datetime import datetime, timedelta ,date
from mx import DateTime

class inherit_res_partner(osv.osv):
    _inherit = "res.partner"
    
    def onchange_parent_id(self, cr, uid, ids, parent_id, context=None):
        def value_or_id(val):
            """ return val or val.id if val is a browse record """
            return val if isinstance(val, (bool, int, long, float, basestring)) else val.id
        if not parent_id or not ids:
            return {'value': {}}
        if parent_id:
            result = {}
            partner = self.browse(cr, uid, ids[0], context=context)
            if partner.parent_id and partner.parent_id.id != parent_id:
                result['warning'] = {
                    'title': _('Warning'),
                    'message': _('Changing the company of a contact should only be done if it '
                                 'was never correctly set. If an existing contact starts working for a new '
                                 'company then a new contact should be created under that new '
                                 'company. You can use the "Discard" button to abandon this change.')}
            # for contacts: copy the parent address, if set (aka, at least
            # one value is set in the address: otherwise, keep the one from
            # the contact)
            if partner.type == 'contact':
                parent = self.browse(cr, uid, parent_id, context=context)
                address_fields = self._address_fields(cr, uid, context=context)
                if any(parent[key] for key in address_fields):
                    result['value'] = dict((key, value_or_id(parent[key])) for key in address_fields)
        return result
    
    def on_change_company_type(self, cr, uid, ids , company_type, context):
        if  company_type == 'company':
            return {'value': {'is_company': company_type == 'company',
                              'is_commercial': False}}
        elif  company_type == 'commercial':
            return {'value': {'is_commercial': company_type == 'commercial',
                              'is_company': False}}
        elif  company_type != 'commercial' and company_type != 'company':
            return {'value': {'is_commercial': False,
                              'is_company': False}}

    
    def onchange_birth_date(self,cr,uid,ids,birth_date,context=None):
        if birth_date:
            now=DateTime.now()
            if birth_date:
                birth_date=DateTime.strptime(birth_date,'%Y-%m-%d')                
                delta=DateTime.Age (now, birth_date) 
                year=str(delta.years)               
            else:
                year = "No DoB !"         
            return {'value': {'age': year}}
    
    _columns = {
            'accomp_id':fields.many2one('travel.insurance.policy', 'Accompanied', required=False),
            'is_client':fields.boolean('Is a Client', required=False),
            'is_commercial':fields.boolean('Commercial', required=False),
            'fixed_rate_commission':fields.boolean('Fixed', required=False), 
            'commission_value': fields.float(''),
            'commission_scales_ids':fields.one2many('commission.scales', 'scale_id', 'Commission Scale', required=False),
            'company_type': fields.selection(
                selection=[('person', 'Individual'),
                           ('company', 'Company'),
                           ('commercial','Commercial')],
                    string='Company Type', select=True),
            'birth_date': fields.date('Date of Birth'),
            'age': fields.integer('Age'),
            'sex':fields.selection([
                ('male', 'Male'),
                ('femele', 'Femele'),
                 ], 'Sex', select=True),
            'pass_number':fields.char('Passeport No'),
            'CIN':fields.char('Identity Card No'),
            'drive_licence':fields.char('Drice Licence No'),
            'register_no':fields.char('Register No'),
            'vat_id':fields.char('VAT ID'),
            'sector_company':fields.char('Sector'),
            'Type':fields.selection([
                ('private', 'Private'),
                ('public', 'Public'),
                 ], 'Type', select=True),
             
                }
    _defaults = {  
        'company_type': 'person',
        }
class commission_scales(osv.osv):
    _name = 'commission.scales' 
    _columns = {
            'scale_id':fields.many2one('res.partner', 'Partner', required=False), 
            'min_turn_over': fields.float('Min Turn Over'),
            'max_turn_over': fields.float('Max Turn Over'),
            'commercial_value': fields.float('Commercial Value'),           
    }