# -*- coding: utf-8 -*-
{
"name": "Sahara Travel Insurance",
"version": "1.0",
"depends": ["base","sahara_insurance_sales"],
"author": "Sahara - MT Consulting HHM, Hdh, MMO,FBL",
"category": "Category",
"description": """ Sahara Travel Insurance """,

"init_xml":[],
'data': [
         
         "data/configuration.xml",
         "views/configuration.xml",
         "views/travel_insurance_policy.xml",
#          "data/sequence.xml",
         'rapport/rapport_travel_insurance_policy.xml',
         'rapport/rapport_travel_insurance_reinsurance.xml',
         'rapport/travel_insurance_policy_workflow.xml',
           ],
'demo': [],
'test': [],
'installable': True,

}
