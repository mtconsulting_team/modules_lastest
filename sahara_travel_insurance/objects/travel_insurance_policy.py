# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp import api, models, _
import time
from datetime import datetime, timedelta , date
from mx import DateTime
import logging
_logger = logging.getLogger(__name__)
from dbus.proxies import _logger

class travel_insurance_policy(models.Model):
    _name = "travel.insurance.policy"

    
    def onchanege_end_date(self, cr, uid, id, start_date, coverage_period, val, context=None):
        if val == 1 :
            start_date = date.today() + timedelta(days=int(1))
            start_date = datetime.strptime(str(start_date), "%Y-%m-%d")
            return {'value' : {'start_date':start_date}}
        if start_date and coverage_period:
            start_date = datetime.strptime(start_date, "%Y-%m-%d")
            end_date = start_date + timedelta(days=int(coverage_period))
            return {'value' : {'end_date' : end_date}}
    def get_end_date(self, cr, uid, ids):
        start_date = datetime.strptime(ids['start_date'], "%Y-%m-%d")
        end_date = start_date + timedelta(days=int(ids['coverage_period']))
        return end_date
    
    def onchange_partner(self, cr, uid, ids, name_insur, context=None):
        value = {}
        partner_obj = self.pool.get('res.partner').browse(cr, uid, name_insur)
        value = {
                    'passport_number':partner_obj.pass_number or '',
                    'phone':partner_obj.phone or '',
                    'birth_date':partner_obj.birth_date or '',
                    'sex':partner_obj.sex or '',
                     }
        return {'value':value} 
         
    def onchange_birth_date(self, cr, uid, ids, birth_date, context=None):
        dob = birth_date
        print "-------------", dob
        if dob:
            now = DateTime.now()
            if dob:
                dob = DateTime.strptime(dob, '%Y-%m-%d')
                delta = DateTime.Age (now, dob)
                year = str(delta.years)
            else:
                year = "No DoB !"
            domain = self.pool.get('configuration.age.domain').search(cr, uid, [('age_max', '>', year), ('age_min', '<', year)])
            if domain:
                return{'value':{'domain_id':domain[0]}}
        if not dob:
            return{'value':{'domain_id':''}}
  
           
    def compute_values(self, cr, uid, ids, context=None):
        values = {
                              'net_premium':0,
                              'tax':0,
                              'adv_fees':0,
                              'stamp_duty':0,
                              'issue_fees':0,
                              'gross_premium':0,
                    }  
        current_obj = self.browse(cr, uid, ids, context)
        values_obj = self.pool.get('scale.insurance')
        values_ids = values_obj.search(cr, uid, [('zone_id', '=', current_obj.zone_id.id), ('domain_id', '=', current_obj.domain_id.id), ('name', '=', current_obj.coverage_period)])
        _logger.warning('_____values____'+str(values_ids))
        if values_ids and len(values_ids) > 0:
                    values_instance = values_obj.browse(cr, uid, values_ids[0], context)
                    values = {
                              'net_premium':values_instance.net_premium,
                              'tax':values_instance.tax,
                              'adv_fees':values_instance.adv_fees,
                              'stamp_duty':values_instance.stamp_duty,
                              'issue_fees':values_instance.issue_fees,
                              'gross_premium':values_instance.gross_premium,
                    }        
                    self.write(cr, uid, ids, values, context)
        return  {'value' : values}

    def _check_date(self, cr, user, ids): 
        obj = self.browse(cr, user, ids[0])
        date = obj.start_date
        if date:
            date = datetime.strptime(date, "%Y-%m-%d")
            if date < date.today() and date:
                    return False
        return True
    _constraints = [(_check_date, 'Error: Coverage Start day must be after today', ['start_date']) ]  
   
    _columns = {
                'name':fields.char('Travel insurance Policy No', required=True),
                'name_insur':fields.many2one('res.partner', "Insured's Name"),
                'passport_number':fields.char('Passport No'),
                'birth_date':fields.date('Date of Birth'),
                'ref_to_commercial':fields.boolean('Ref to a commercial', required=False),
                'commercial_id':fields.many2one('res.partner', "", required=False),
                'age':fields.date('Age'),
                'sex':fields.selection([
                    ('male', 'Male'),
                    ('femele', 'Femele'),
                     ], 'Sex', select=True),
                'phone':fields.char('Phone No'),
                'zone_id':fields.many2one('configuration.zone', 'Zone'),
                'domain_id':fields.many2one('configuration.age.domain', 'Age'),
                'coverage_period':fields.selection([
                    ('10', '10'),
                    ('20', '20'),
                    ('30', '30'),
                    ('45', '45'),
                    ('90', '90'),
                    ('180', '180'),
                    ('365', '365'),
                     ], 'Period Of Coverage', select=True),
                'start_date':fields.date('From'),
                'end_date':fields.date('To'),
                'covered_person_ids':fields.one2many('res.partner', 'accomp_id', 'Accompanied'),
                'net_premium':fields.float('Net Premium', digits=(16, 3)),
                'tax':fields.float('Tax', digits=(16, 3)),
                'adv_fees':fields.float('Advisory Fees', digits=(16, 3)),
                'stamp_duty':fields.float('Stamp Duty', digits=(16, 3)),
                'issue_fees':fields.float('Issue Fees', digits=(16, 3)),
                'gross_premium':fields.float('Gross Premium', digits=(16, 3)),
                'code':fields.char('Office No'),
                'valid':fields.boolean('valid'),
                'state':fields.selection([
                    ('draft', 'Draft'),
                    ('confirm', 'Confirm'),
                    ('paid', 'Pay'),
                    ('done', 'Done'),
                     ], 'Status', select=True, default='draft'),
               
                }
    # track_visibility='onchange'
    _defaults = {   
            'start_date': date.today().strftime('%Y-%m-%d'),
            'name': lambda obj, cr, uid, context: '/',
            'state':'draft',
        }
    def validate(self, cr, uid, ids, *args):
#         vals['valid']=True
#         return super(travel_insurance_policy, self).write(cr, uid, vals, context=context)
        return self.write(cr, uid, ids, {'state':'paid'})
    
    def payer(self, cr, uid, vals, context=None):
        
        if context is None:
            context = {}
        current_obj = self.browse(cr, uid, vals, context)

        if current_obj.name == '/':
            code = self.pool.get('res.users').browse(cr, uid, uid).company_id.code
        if not code:
            code = "01"
        
        values = {
                  'name':str(code) + self.pool.get('ir.sequence').get(cr, uid, 'travel.policy', context=context) or '/',
                  'code':str(code),
                  'state':'done',
                }        
        return self.write(cr, uid, vals, values, context)
    
    @api.multi
    def print_insurance(self):
        return self.env['report'].get_action(self, 'sahara_travel_insurance.rapport_travel_insurance_policy')

    def create(self, cr, uid, vals, context=None):

        vals['state'] = 'confirm'
        return super(travel_insurance_policy, self).create(cr, uid, vals, context=context)
    
class res_company(osv.osv):
    _inherit = 'res.company' 
    
    _columns = {
                'code':fields.char('code'),
                }
class sahara_res_partner(osv.osv):
    _inherit = 'res.partner' 
    def get_parent_pass_num(self, cr, uid, id,passport_number, context=None):
        return {'value' : {'pass_number' : passport_number}}
    
class travel_insurance_reinsurance(osv.osv):
    _name = 'travel.insurance.reinsurance' 
    _inherit = 'travel.insurance.policy' 
    # Do not touch _name it must be same as _inherit
    # _name = 'travel.insurance.policy'
    _columns = {
             'start_date2':fields.date('From'),
             'end_date2':fields.date('To'),
    }
    @api.multi
    def print_reinsurance(self):
        return self.env['report'].get_action(self, 'sahara_travel_insurance.rapport_travel_insurance_reinsurance')
