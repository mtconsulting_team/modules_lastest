# -*- coding: utf-8 -*-
from openerp.osv import osv,fields

class configuration_age_domain(osv.osv):
    _name="configuration.zone"
    _columns = {
                'name':fields.char('Zone'),
                'scale_insurance_ids':fields.one2many('scale.insurance','zone_id', 'Insurance Scale')

                }



class configuration_zone(osv.osv):
    _name="configuration.age.domain"
    _columns = {
                'name':fields.char('Domaine'),
                'age_min':fields.integer('Min Age'),
                'age_max':fields.integer('Max Age'),
                'scale_insurance_ids':fields.one2many('scale.insurance','domain_id', 'Insurance Scale')
                
                }
    
class scale_insurance(osv.osv):
    _name="scale.insurance"
    _columns = {
                'zone_id':fields.many2one('configuration.zone', 'Zone', required=True),
                'domain_id':fields.many2one('configuration.age.domain', 'Age Domain', required=True), 
                'name':fields.char('Period(Days)'),
                'net_premium':fields.float('Net Premium'),
                'tax':fields.float('Tax'),
                'adv_fees':fields.float('Advisory Fees'),
                'stamp_duty':fields.float('Stamp Duty'),
                'issue_fees':fields.float('Issue Fees'),
                'gross_premium':fields.float('Gross Premium'),
                'reinsurance_value': fields.float('Re/Insurance Value'),
                        }
