#  -*- coding: utf-8 -*-

{
    "name": "Libya Cities",
    "version": "0.1",
    "author": "MT-Consulting",
    "category": 'Cities',
    'complexity': "easy",
    "description": """
        Search for Tunisien zip codes and cities 
   
    """,
    'website': 'http://www.mtconsulting.odoo.com',
    'images': [],
    'init_xml': [],
     "depends": ['mt_cities'],
    'data': [
                   "datas/mt_country_state_data.xml",
                   
                   
                   
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
